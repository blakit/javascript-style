Description
---
Contains shareable ESLint config

Installation
---
`npm install --save-dev eslint-config-blakit eslint eslint-config-airbnb eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-import`

Usage
---
Put code snippet in `.eslintrc`

```json
{
    "extends": "blakit"
}
```

Add to package.json at "scripts" section

```json
  "lint": "./node_modules/.bin/eslint ./src",
```

next run

```cmd
npm run lint
```

Using with webpack 3.x
---

Install loader

```cmd
npm install --save-dev eslint-loader
```

Add rule to webpack.config.js

```js
module: {
    rules: [
        {
            enforce: 'pre',
            test: /\.js$/,
            exclude: /node_modules/,
            use: ['eslint-loader']
        },
    ]
}
```
